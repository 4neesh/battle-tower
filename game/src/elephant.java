
public class elephant extends enemy {
    private int health = 10;
    private int entryStage;
    public int position;
    private int ready;

    public elephant(String name, int entrypoint) {
        this.name = name; this.entryStage = entrypoint;
    }

    public boolean movementReady(int movementCount) {
        if (this.ready == 0) {
            this.ready += movementCount;
            return false;
        } else {
            this.ready = 0;
            return true;
        }
    }

    public int hit(int reduceHealthBy) {
        this.health -= reduceHealthBy;
        if (this.health<0){
            return 0;
        }
        else {
            return this.health;
        }
    }


    public int getPosition() {
        return this.position;
    }

    public int getEntry() {
        return this.entryStage;
    }

    public void setPosition(int increasePositionBy) {
        this.position += increasePositionBy;
    }
}
