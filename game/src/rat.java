
public class rat extends enemy {
    private int health = 1;
    private int entryStage;
    public int position;
    private String name;

    public rat(String name, int entryPoint) {
        this.name = name; this.entryStage = entryPoint;
    }

    public String getName(){return this.name;}

    public int getPosition() {
        return this.position;
    }

    public int getEntry() {
        return this.entryStage;
    }

    public void setPosition(int entryPoint) {
        this.position += entryPoint;
    }

    public int hit(int health) {
        this.health -= health;
        return this.health;
    }
}
