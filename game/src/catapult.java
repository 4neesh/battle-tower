

import java.util.ArrayList;

public class catapult extends tower {
    int position;
    int damage = 5;
    int firing = 0;

    public catapult(int ready, int position) {
        this.position = position;
        ArrayList attackPos = new ArrayList();

        for(int i = 0; i < this.position + 1; ++i) {
            attackPos.add(i);
        }

    }

    public int getDamage() {
        return this.damage;
    }

    public int getPosition() {
        return this.position;
    }

    public int willFire() {
        firing++;
        if (firing<=4) {
            return 4 - firing;
        }
        else{
            return 0;
        }
    }
    public int returnFiring(){
        return firing;
    }
    public void setFiring(){
        firing = 0;
    }

}
