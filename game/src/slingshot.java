public class slingshot extends tower {
    private int position;
    private int damage = 1;
    int firing = 0;

    public slingshot(int ready, int position) {
        this.position = position;
    }

    public String getTower() {
        return "slingshot";
    }

    public int getDamage() {
        return this.damage;
    }

    public int getPosition() {
        return this.position;
    }

    public int willFire() {
        return firing;
    }
}
