

public class tower {
    private int damage;
    private int position;
    boolean fire;
    int firing;
    int spread = 4;

    public tower() {
    }

    public int getDamage() {
        return this.damage;
    }

    public int getPosition() {
        return this.position;
    }

    public String getTower() {
        return "tower";
    }

    public int willFire() {
        return firing;
    }
    public int getSpread(){
        return this.spread;
    }
}
