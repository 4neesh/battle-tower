
import java.util.ArrayList;
import java.util.Scanner;

public class game {
    public game() {
    }

    public static void main(String[] var0) {
        int stage = 0;

        Scanner numberOfSlings = new Scanner(System.in);
        Scanner numberOfCatapults = new Scanner(System.in);

        System.out.println("Welcome to the battle-tower game!\n\nLet's set-up your defence towers;\n\nEnter the number of slingshots you would like to add:");
        int slingsToMake = numberOfSlings.nextInt();
        System.out.println("Now enter the number of Catapults you would like to add:");
        int CatapultsToMake = numberOfCatapults.nextInt();

        tower[] towerList = new tower[slingsToMake + CatapultsToMake];
        for (int i = 0; i<slingsToMake; i++){
            Scanner slingPositionInput = new Scanner(System.in);
            System.out.println("Enter the position for your slingshot (id: " + i + ")");
            int userDefinedPosition = slingPositionInput.nextInt();
            towerList[i] = new slingshot(1,userDefinedPosition);
        }

        for (int j = slingsToMake; j<slingsToMake + CatapultsToMake; j++){
            Scanner catapultPositionInput = new Scanner(System.in);
            System.out.println("Enter the position for your Catapult (id: " + j + ")");
            int userDefinedPosition = catapultPositionInput.nextInt();
            towerList[j] = new catapult(1,userDefinedPosition);
        }

        //randomly create enemies
        int numOfRats = (int)(Math.random() * 7 + 1);
        int numOfElephants = (int)(Math.random() * 4 + 1);
        enemy[] enemyList = new enemy[ numOfRats + numOfElephants];
        for (int i =0; i<numOfRats; i++){
            int ratRan = ((int)(Math.random() * 10));
            enemyList[i] = new rat("Rat(id: "+i+")",ratRan);
        }
        for (int i =numOfRats; i<numOfElephants + numOfRats; i++){
            int eleRan = ((int)(Math.random() * 10));
            enemyList[i] = new elephant("Elephant(id: "+i+")",eleRan);
        }

        advance(towerList, enemyList, stage);
    }

    private static void advance(tower[] tower1, enemy[] enemyList, int stage) {

        System.out.println("Move: " + stage);
        tower[] tower = tower1;
        int towerPos = tower1.length;

        //loop through each tower and give it's position, attackList and readiness
        int i;
        ArrayList<Integer> attackList;
        int j;
        tower currentTower;
        for (i = 0; i < towerPos; ++i) {
            currentTower = tower[i];
            attackList = new ArrayList<>();
            for (j = 0; j < currentTower.getSpread(); ++j) {
                if (currentTower.getPosition() -j >=0) {
                    attackList.add(currentTower.getPosition() - j);
                }
            }
            System.out.println(currentTower.getClass() + " at position " + currentTower.getPosition() + " attacks positions " + attackList);
            if (currentTower instanceof catapult) {

                System.out.println(currentTower.getClass() + " will be ready to fire in  " + currentTower.willFire() + " moves.");

            }
        }

        enemy selectedEnemy;

        for (i = 0; i < enemy.getCounter(); i++) {
            selectedEnemy = enemyList[i];
            if (selectedEnemy.getEntry() <= stage && selectedEnemy.hit(0) > 0) {
                selectedEnemy.position = 0;
                System.out.println(selectedEnemy.getName() + " is at position " + selectedEnemy.getPosition() + " with health of " + selectedEnemy.hit(0));
            }
        }

        tower = tower1;
        towerPos = tower1.length;


        //attack the enemies from the towers
        for (i = 0; i < towerPos; ++i) {
            currentTower = tower[i];
            if (currentTower instanceof catapult) {
                attackList = new ArrayList<>();
                for (j = 0; j < currentTower.getSpread(); j++) {
                    attackList.add(currentTower.getPosition() - j);
                    for (enemy currentEn : enemyList) {
                        if ((currentEn.getPosition() == currentTower.getPosition() - j) && (currentEn.hit(0) > 0) && (((catapult) currentTower).returnFiring() >= 4) && (currentEn.getEntry()<=stage)) {
                            System.out.println(currentTower.getClass() + " attacks " + currentEn.getClass() + " at position " + (currentTower.getPosition() - j) + " inflicting damage of " + currentTower.getDamage() + ".");
                            if (currentEn instanceof rat) {
                                currentEn.hit(currentTower.getDamage());
                                System.out.println(currentEn.getName() + " has health of " + currentEn.hit(0) + " and is eliminated.");

                                ((catapult) currentTower).setFiring();
                            } else if (currentEn instanceof elephant) {
                                currentEn.hit(currentTower.getDamage());
                                System.out.println(currentEn.getName() + " has remaining health of " + currentEn.hit(0));
                                ((catapult) currentTower).setFiring();
                            }
                        }
                    }
                }

            } else {
                attackList = new ArrayList<>();
                for (j = 0; j <= currentTower.getSpread(); j++) {
                    attackList.add(currentTower.getPosition() - j);

                    for (enemy currentEn : enemyList) {
                        if (currentEn.getPosition() == currentTower.getPosition() - j && currentEn.hit(0) > 0 && (currentEn.getEntry()<=stage)) {
                            System.out.println(currentTower.getClass() + " attacks " + currentEn.getClass() + " at position " + (currentTower.getPosition() - j)+ " inflicting damage of " + currentTower.getDamage() + ".");
                            if (currentEn instanceof rat) {
                                currentEn.hit(currentTower.getDamage());
                                System.out.println(currentEn.getName() + " is eliminated.");
                            } else if (currentEn instanceof elephant) {
                                currentEn.hit(currentTower.getDamage());
                                String addedInfo;
                                if(currentEn.hit(0)<=0){
                                    addedInfo = " and is eliminated.";}
                                else{
                                    addedInfo = ".";}
                                System.out.println(currentEn.getName() + " has health of " + currentEn.hit(0) + addedInfo);
                            }
                        }
                    }
                }
            }

            attackList = new ArrayList<>();

            for (j = 0; j < currentTower.getSpread(); j++) {
                attackList.add(currentTower.getPosition() - j);
            }
        }


        int maxMoves = 30;

        //either end the game due to winning, losing, or continue the game.
        for (int k = 0; k < enemy.getCounter(); k++) {
            enemy currentEnemy = enemyList[k];
            if (currentEnemy.getPosition() == 10) {
                System.out.println("------------------------------\n\nYou lose! " + currentEnemy.getName() + " invaded your territory (pos: 10.)");
                System.exit(0);
            }
        }

        for (int l = 0; l < enemy.getCounter(); l++) {
            enemy standingEnemy = enemyList[l];
            {
                if (stage == maxMoves) {
                    System.out.println("------------------------------\n\nThe enemy has not reached the end of the corridor in " + maxMoves + " moves so the game is won");
                    System.exit(0);

                } else {
                    towerPos = enemyList.length;

                    for (i = 0; i < towerPos; i++) {
                        standingEnemy = enemyList[i];
                        if (standingEnemy instanceof rat && standingEnemy.getEntry() <= stage && standingEnemy.hit(0) > 0) {
                            standingEnemy.setPosition(2);
                        } else if (standingEnemy instanceof elephant && (standingEnemy.getEntry() <= stage) && (standingEnemy.hit(0) > 0)) {
                            if (standingEnemy.movementReady(1)) {
                                standingEnemy.setPosition(1);
                                standingEnemy.movementReady(1);
                                standingEnemy.movementReady(1);
                            } else {
                                standingEnemy.movementReady(1);
                                standingEnemy.movementReady(1);
                            }
                        }
                    }
                    stage++;
                    System.out.println("------------------------------");
                    advance(tower1, enemyList, stage);
                }
            }
        }
    }
}
